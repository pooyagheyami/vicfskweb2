#In the name of God
#import string
import os

class Txtparse:
    def __init__(self):
        self.author = ''
        self.title = ''
        self.contxt = ''
    def opentxt(self,txtfile):
        #print(txtfile)
        with open(txtfile,'r',encoding='utf-8') as fil :
            self.lines = fil.readlines()


    #parse text file
    def readtxt(self):
        self.author = self.lines[0].strip('\n')
        jom1 = self.lines[1].split('.')
        sss = ''
        if len(jom1) > 1:
            for j in jom1:
                if len(sss)+len(j) > 92:
                    sss = sss + j[:(92-len(sss))]
                    break
                else:
                    sss = sss + j
            self.title = sss + '...'
        else:
            self.title = jom1[0]
        for i in range(2, len(self.lines)):
            self.contxt = self.contxt + self.lines[i]
        #print(self.contxt)

def Get_All_Text(Pathfile):
    res = []
    #itxt = Txtparse()
    listfile = Get_files(Pathfile)

    for files in listfile:
        itxt = Txtparse()
        itxt.opentxt(Pathfile+'/'+files)
        itxt.readtxt()
        res.append([itxt.author,itxt.title,itxt.contxt])
    return res

def Get_files(txtpath):
    return os.listdir(txtpath)